# Compass

Contains CD REDs components repository as confing-as-code for Atlassian Compass 

# What is component

A Compass component represents a piece of software, like a microservice, library, or application. Together, they make up your software architecture and are cataloged for free in Compass, a developer experience tool.

https://support.atlassian.com/jira-software-cloud/docs/what-are-compass-components/

# confing-as-code

Configuration as code (config as code) allows you to manage Compass component data via a YAML file that is stored in a git repository. Config as code requires the Bitbucket, GitHub, or GitLab app to be installed and configured on your Compass site. With config as code, you declare component details in a compass.yaml file that sits alongside the source code or configuration for the component itself. The component then becomes a managed component in Compass. You can manage existing components this way or create new components in Compass by simply committing a compass.yaml file.

https://developer.atlassian.com/cloud/compass/config-as-code/manage-components-with-config-as-code/